﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CursProject_SPO_WPF
{
    class DeletingFile
    {
        FileInfo file;
        public DeletingFile(string path)
        {
            if (path == null)
                throw new ArgumentNullException();

            this.file = new FileInfo(path);

            if (!file.Exists)
                throw new Exception("Заданный файл не существует!");
        }
        private void Renaming()
        {
            char[] symb = new char[82];
            for (int i = 33, j = 1; i <= 126 && j < symb.Length; i++, j++)
            {
                if (i != 34 && i != 42 && i != 47 && i != 58 && i != 60
                    && i != 62 && i != 63 && i != 92 && i != 124)
                {
                    symb[j] = (char)i;
                }
            }

            int ind = file.FullName.LastIndexOf('\\');

            file.MoveTo(file.FullName.Remove(ind + 1) + new String(symb[new Random().Next(1, symb.Length)], 10) + 
                '.' + new String(symb[new Random().Next(1, symb.Length)], 3));
            for (int i = 97; i <= 122; i++)
            {
                file.MoveTo(file.FullName.Remove(ind + 1) + new String((char)i, 10) + '.' + new String((char)i, 3));
            }
            file.MoveTo(file.FullName.Remove(ind + 1) + new String(symb[new Random().Next(1, symb.Length)], 10) + 
                '.' + new String(symb[new Random().Next(1, symb.Length)], 3));
        }
        public void PeterGutmannAlgorithm()
        {
            using (FileStream fileStream = new FileStream(file.FullName, FileMode.Create, FileAccess.Write))
            {
                byte[] randomDataArray = new byte[file.Length];
                byte[] dataArray = new byte[] { 0x55, 0x55, 0x55, 
                                                0xAA, 0xAA, 0xAA, 
                                                0x92, 0x49, 0x24, 
                                                0x49, 0x24, 0x92, 
                                                0x00, 0x00, 0x00, 
                                                0x11, 0x11, 0x11,
                                                0x22, 0x22, 0x22,
                                                0x33, 0x33, 0x33,
                                                0x44, 0x44, 0x44,
                                                0x55, 0x55, 0x55,
                                                0x66, 0x66, 0x66,
                                                0x77, 0x77, 0x77,
                                                0x88, 0x88, 0x88,
                                                0x99, 0x99, 0x99,
                                                0xAA, 0xAA, 0xAA,
                                                0xBB, 0xBB, 0xBB,
                                                0xCC, 0xCC, 0xCC,
                                                0xDD, 0xDD, 0xDD, 
                                                0xEE, 0xEE, 0xEE,
                                                0xFF, 0xFF, 0xFF,
                                                0x92, 0x49, 0x24, 
                                                0x49, 0x24, 0x92,
                                                0x24, 0x92, 0x49,
                                                0x6D, 0xB6, 0xDB,
                                                0xB6, 0xDB, 0x6D,
                                                0xDB, 0x6D, 0xB6 };

                for (int i = 0; i < 4; i++)
                {
                    new Random().NextBytes(randomDataArray);

                    for (int j = 0; j < file.Length; j++)
                    {
                        fileStream.WriteByte(randomDataArray[j]);
                    }

                    fileStream.Seek(0, SeekOrigin.Begin);
                }

                for (int i = 0; i < dataArray.Length; i++)
                {
                    for (int j = 0; j < file.Length; j++)
                    {
                        fileStream.WriteByte(dataArray[i]);
                    }

                    fileStream.Seek(0, SeekOrigin.Begin);
                }

                for (int i = 0; i < 4; i++)
                {
                    new Random().NextBytes(randomDataArray);

                    for (int j = 0; j < file.Length; j++)
                    {
                        fileStream.WriteByte(randomDataArray[j]);
                    }

                    fileStream.Seek(0, SeekOrigin.Begin);
                }
                fileStream.Flush();
            }
            this.Renaming();
            file.Delete();
        }
    }
}
