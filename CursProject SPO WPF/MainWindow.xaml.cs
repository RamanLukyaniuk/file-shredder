﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CursProject_SPO_WPF
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string path;
        string derictoryPath;
        IEnumerable<string> collectionPath;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void ButtonSelectDirectory_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (RadioButtonFile.IsChecked == true)
                {
                    OpenFileDialog config = new OpenFileDialog();
                    bool? open = config.ShowDialog();

                    if (open == true)
                    {
                        path = config.FileName;
                        TextBoxDirectory.Text = config.FileName;

                        ButtonDelete.IsEnabled = true;
                    }
                }
                else
                {
                    System.Windows.Forms.FolderBrowserDialog FBD = new System.Windows.Forms.FolderBrowserDialog();

                    if (FBD.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        TextBoxDirectory.Text = FBD.SelectedPath;
                        derictoryPath = FBD.SelectedPath;
                        collectionPath = Directory.EnumerateFiles(FBD.SelectedPath, "*.*", SearchOption.AllDirectories);

                        foreach (string currentFile in Directory.EnumerateFileSystemEntries(FBD.SelectedPath, "*.*", SearchOption.AllDirectories))
                        {
                            ListBoxContentDirectory.Items.Add(currentFile);
                        }

                        ButtonDelete.IsEnabled = true;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ButtonDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MessageBoxResult confirm = MessageBox.Show("Вы действительно хотите безвозвратно удалить эти данные?", 
                    "Подтвердить", MessageBoxButton.YesNo);

                if (confirm == MessageBoxResult.Yes)
                {
                    if (RadioButtonFile.IsChecked == true)
                    {
                        DeletingFile delFile = new DeletingFile(path);
                        delFile.PeterGutmannAlgorithm();
                    }
                    else
                    {
                        foreach (string currentFile in collectionPath)
                        {
                            ListBoxContentDirectory.Items.Add(currentFile);

                            DeletingFile delFile = new DeletingFile(currentFile);
                            delFile.PeterGutmannAlgorithm();
                        }

                        Directory.Delete(derictoryPath, true);
                    }

                    TextBoxDirectory.Text = string.Empty;
                    ListBoxContentDirectory.Items.Clear();
                    ButtonDelete.IsEnabled = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ButtonClear_Click(object sender, RoutedEventArgs e)
        {
            TextBoxDirectory.Text = string.Empty;
            ListBoxContentDirectory.Items.Clear();
            ButtonDelete.IsEnabled = false;
        }

        private void RadioButtonCatalog_Checked(object sender, RoutedEventArgs e)
        {
            TextBoxDirectory.Text = string.Empty;
            ListBoxContentDirectory.Items.Clear();
            ButtonDelete.IsEnabled = false;
        }

        private void RadioButtonFile_Checked(object sender, RoutedEventArgs e)
        {
            //TextBoxDirectory.Text = string.Empty;
            //ListBoxContentDirectory.Items.Clear();
            //ButtonDelete.IsEnabled = false;
        }
    }
}
